// APP "mad_control" FOR MAD COMPANY
// (C) 2021, dev: <weendibol21300@gmail.com>

// ## FLUTTER PACKEAGES
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart' show DeviceOrientation, SystemChrome;

// ## IMPORT MODULES
import 'package:mad_control/pages/start.page.dart';

// ## MAIN FUNCTIONS
void main(){
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MadApp());
}

// ## USE MATERIAL APP
class MadApp extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: StartPages(),
    );
  }
}