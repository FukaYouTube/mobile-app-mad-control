// ## BASIC DART PACKEAGES
import 'dart:convert';
import 'dart:ui';

// ## FLUTTER PACKEAGES
import 'package:flutter/material.dart';

// ## IMPORT PUB PACKAGES
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

// ## CONNECTION BLUETOOTH MODULE DATA
var connection;

// ## OPTIONS
String _address = '00:21:07:00:52:AC'; // ## Mac address bluetooth module arduino
String _logoPath1 = 'assets/images/logo1.png'; // ## Path logo for home page
String _logoPath2 = 'assets/images/logo2.png'; // ## Path logo for control page
String _backgroundImagePath = 'assets/images/bg.png'; // ## Path background image

// BUTTONS
bool isClick1 = false;
bool isClick2 = false;
bool isClick3 = false;
bool isClick4 = false;
bool isClick5 = false;
bool isClick6 = false;
bool isClick7 = false;

// ## HOME PAGES
class HomePage extends StatefulWidget {
  @override
  _HomePage createState() => _HomePage();
}

class _HomePage extends State <HomePage> {
  @override
  Widget build(BuildContext context){
    return FutureBuilder(
      future: FlutterBluetoothSerial.instance.requestEnable(),
      builder: (ctx, future){
        if(future.connectionState == ConnectionState.waiting){
          return Scaffold(
            body: Container(
              height: double.infinity,
              child: Center(
                child: Icon(Icons.bluetooth_disabled, size: 200.0, color: Colors.blue)
              )
            )
          );
        }else if(future.connectionState == ConnectionState.done){
          return Home();
        }else{
          return Home();
        }
      },
    );
  }
}

class Home extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> with WidgetsBindingObserver {
  BluetoothState _bluetoothState = BluetoothState.UNKNOWN;
  List<BluetoothDevice> devices = [];

  @override
  void initState(){
    super.initState();

    WidgetsBinding.instance?.addObserver(this);

    _getBluetoothState();
    _stateChangeListener();
  }

  @override
  void dispose(){
    super.dispose();
    WidgetsBinding.instance?.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state){
    if(state.index == 0){
      if(_bluetoothState.isEnabled){
        _listBondedDevices();
      }
    }
  }

  _getBluetoothState(){
    FlutterBluetoothSerial.instance.state.then((state){
      _bluetoothState = state;

      if(_bluetoothState.isEnabled){
        _listBondedDevices();
      }

      setState((){});
    });
  }

  _stateChangeListener(){
    FlutterBluetoothSerial.instance.onStateChanged().listen((BluetoothState state){
      _bluetoothState = state;

      if(_bluetoothState.isEnabled){
        _listBondedDevices();
      }else{
        devices.clear();
      }

      print('State isEnabled: ${state.isEnabled}');
      setState((){});
    });
  }

  _listBondedDevices(){
    FlutterBluetoothSerial.instance.getBondedDevices().then((List<BluetoothDevice> bondedDevices){
      devices = bondedDevices;
      setState((){});
    });
  }

  @override
  Widget build(BuildContext context){
    return Container(
      decoration: BoxDecoration(
        color: Colors.white24,
        image: DecorationImage(
          image: AssetImage(_backgroundImagePath),
          fit: BoxFit.cover
        )
      ),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 1, sigmaY: 1),
        child: Center(
          child: SizedBox(
            width: 250,
            child: ElevatedButton(
              child: Image(image: AssetImage(_logoPath1), width: 180),
              style: ElevatedButton.styleFrom(
                primary: Color.fromRGBO(255, 255, 255, .8),
                padding: EdgeInsets.all(24)
              ),
              onPressed: () async {
                try {
                  connection = await BluetoothConnection.toAddress(_address);

                  Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    return MadControl();
                  }));
                }catch(error){
                  print(error);

                  Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    return MadControl();
                  }));
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}

// ## MAD CONTROL PAGES
class MadControl extends StatefulWidget {
  @override
  _MadControl createState() => _MadControl();
}

class _MadControl extends State<MadControl> {
  void _sendData(String text) async {
    text = text.trim();

    if(text.length > 0){
      try {
        connection.output.add(utf8.encode(text));
        await connection.output.allSent;
      }catch(error){
        print(error);
        setState((){});
      }
    }
  }

  @override
  Widget build(BuildContext context){
    
    // ## Display view UX options
    double fontSized1 = 18.0 * MediaQuery.of(context).size.width / 446.0;
    double fontSized2 = 17.0 * MediaQuery.of(context).size.width / 446.0;

    TextStyle style25 = new TextStyle(
      inherit: true,
      fontSize: fontSized1,
      color: Colors.white,
      fontWeight: FontWeight.w300
    );
    TextStyle style24 = new TextStyle(
      inherit: true,
      fontSize: fontSized2,
      color: Colors.white,
      fontWeight: FontWeight.w300
    );

    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: [0.1, 2],
          colors: [
            Color.fromRGBO(135, 206, 235, 1),
            Color.fromRGBO(193, 59, 120, 1)
          ],
        ),
      ),

      child: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 50, top: 50),
                  alignment: Alignment.center,
                  child: Image(image: AssetImage(_logoPath2), width: 220),
                ),

                // #################################################### * #
                // #################### BUTTONS ####################### * #
                // #################################################### * #

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: SizedBox(
                        width: 160,
                        height: 165,
                        child: TextButton(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(padding: EdgeInsets.only(bottom: 25), child: Icon(Icons.home, color: Colors.white, size: 40)),
                              Text('Сдача 2010 году', textAlign: TextAlign.center, style: style25),
                            ]
                          ),
                          style: TextButton.styleFrom(
                            primary: Colors.transparent,
                            side: BorderSide(width: 0.2, color: Colors.white)
                          ),
                          onPressed: (){
                            isClick1 = !isClick1;
                            isClick1 ? _sendData('a') : _sendData('b');
                          },
                        ),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: SizedBox(
                        width: 160,
                        height: 165,
                        child: TextButton(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(padding: EdgeInsets.only(bottom: 25), child: Icon(Icons.home, color: Colors.white, size: 40)),
                              Text('Сдача 2015 году', textAlign: TextAlign.center, style: style25),
                            ]
                          ),
                          style: TextButton.styleFrom(
                            primary: Colors.transparent,
                            side: BorderSide(width: 0.2, color: Colors.white)
                          ),
                          onPressed: (){
                            isClick2 = !isClick2;
                            isClick2 ? _sendData('c') : _sendData('d');
                          },
                        ),
                      ),
                    )
                  ]
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: SizedBox(
                        width: 160,
                        height: 165,
                        child: TextButton(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(padding: EdgeInsets.only(bottom: 25), child: Icon(Icons.home, color: Colors.white, size: 40)),
                              Text('13 домов', textAlign: TextAlign.center, style: style25),
                            ]
                          ),
                          style: TextButton.styleFrom(
                            primary: Colors.transparent,
                            side: BorderSide(width: 0.2, color: Colors.white)
                          ),
                          onPressed: (){
                            isClick3 = !isClick3;
                            isClick3 ? _sendData('e') : _sendData('f');
                          },
                        ),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: SizedBox(
                        width: 160,
                        height: 165,
                        child: TextButton(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(padding: EdgeInsets.only(bottom: 25), child: Icon(Icons.home, color: Colors.white, size: 40)),
                              Text('1 очередь', textAlign: TextAlign.center, style: style25),
                            ]
                          ),
                          style: TextButton.styleFrom(
                            primary: Colors.transparent,
                            side: BorderSide(width: 0.2, color: Colors.white)
                          ),
                          onPressed: (){
                            isClick4 = !isClick4;
                            isClick4 ? _sendData('g') : _sendData('k');
                          },
                        ),
                      ),
                    )
                  ]
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: SizedBox(
                        width: 160,
                        height: 165,
                        child: TextButton(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(padding: EdgeInsets.only(bottom: 25), child: Icon(Icons.home, color: Colors.white, size: 40)),
                              Text('2 очередь', textAlign: TextAlign.center, style: style25),
                            ]
                          ),
                          style: TextButton.styleFrom(
                            primary: Colors.transparent,
                            side: BorderSide(width: 0.2, color: Colors.white)
                          ),
                          onPressed: (){
                            isClick5 = !isClick5;
                            isClick5 ? _sendData('l') : _sendData('m');
                          },
                        ),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: SizedBox(
                        width: 160,
                        height: 165,
                        child: TextButton(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(padding: EdgeInsets.only(bottom: 25), child: Icon(Icons.home, color: Colors.white, size: 40)),
                              Text('3 очередь', textAlign: TextAlign.center, style: style25),
                            ]
                          ),
                          style: TextButton.styleFrom(
                            primary: Colors.transparent,
                            side: BorderSide(width: 0.2, color: Colors.white)
                          ),
                          onPressed: (){
                            isClick6 = !isClick6;
                            isClick6 ? _sendData('o') : _sendData('n');
                          },
                        ),
                      ),
                    )
                  ]
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: SizedBox(
                        width: 160,
                        height: 165,
                        child: TextButton(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(padding: EdgeInsets.only(bottom: 25), child: Icon(Icons.light_mode_outlined, color: Colors.white, size: 40)),
                              Text('Дворовое освещение', textAlign: TextAlign.center, style: style25),
                            ]
                          ),
                          style: TextButton.styleFrom(
                            primary: Colors.transparent,
                            side: BorderSide(width: 0.2, color: Colors.white)
                          ),
                          onPressed: (){
                            isClick7 = !isClick7;
                            isClick7 ? _sendData('r') : _sendData('p');
                          },
                        ),
                      ),
                    ),
                  ]
                ),
              ],
            ),
          ),
        ),
      )
    );
  }
}