// ## BASIC DART PACKEAGES
import 'dart:async';

// ## FLUTTER PACKEAGES
import 'package:flutter/material.dart';

// ## IMPORT MODULES
import 'package:mad_control/pages/home.page.dart';

// ## START PAGES || VIEW LOGO ANIMATION
class StartPages extends StatefulWidget {
  @override
  _StartPagesState createState() => _StartPagesState();
}

class _StartPagesState extends State <StartPages> {
  @override
  void initState(){
    super.initState();

    Timer(Duration(seconds: 5), () => {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (BuildContext context) => HomePage()
      ))
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Image.asset('assets/gif/MAD LOGO ANIMATIONS.gif', width: 160)
      )
    );
  }
}